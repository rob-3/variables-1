# Introduction
Now that you've written your first program, we are going to delve into some of
the core concepts in Python. If you've programmed before, many of the ideas in
the coming sections will likely be familiar to you.

# Variables
Perhaps the most fundamental concept in programming is the variable. A simple
*variable initialization* is as follows:

```
my_variable = 5
```

Note the traditional way to name Python variables: all lowercase letters and
underscore separated.

Variables can be *accessed* simply by using their name:

```
print(my_variable)
# prints 5
```

Variables can have a variety of *types*. In the "hello, world!" program, we saw
the `string` type. Strings are simply sequences of characters or letters. The
following stores `'hello, world!'` into a variable called `my_string`:

```
my_string = 'hello, world!'
```

# Task
Write a program to store the number `42` into a variable called `the_answer`,
and then print out the value of `the_answer`.
